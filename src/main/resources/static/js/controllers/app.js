
app.controller("AppCtrl", function ($scope, $state) {
    $scope.app = {
        name: "学生选课管理系统 "
    };
    $scope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            if (toState.name !== 'signin' && !sessionStorage.getItem('token')) {
                event.preventDefault();
                $state.go('signin');
            }
        })
});
